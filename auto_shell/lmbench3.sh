#!/bin/bash
echo '请输入程序解压路径,必须在后面+/
[默认]/MysqlShell/'
read dir
if [[ -z "$dir" ]];then
        dir='/MysqlShell/'
fi
if [[ ! -d "$dir" ]];then
	echo 'create dir'
	mkdir -p $dir
else
	echo 'dir exits'
fi
rm -rf ${dir}/lmbench3
echo "正在解压源码到${dir}"
tar -zxf my_linux_test_tools/Source_code/lmbench3.tar.gz -C $dir
mkdir -p ${dir}/lmbench3/SCCS
touch ${dir}/lmbench3/SCCS/s.ChangeSet
make -C ${dir}/lmbench3/
echo '即将进入自动化应答程序,当出现Editor [default vi]提示，并进入vi编辑的时候，
请保存内容退出即可开始测试,确认请按回车继续'
read dd
expect -c "
make -C ${dir}lmbench3/results see
expect \"MULTIPLE COPIES [default 1]\"
send \"\r\"
expect \"Job placement selection:\"
send \"1\"
expect \"MB [default 2639]\"
send \"\"
expect \"SUBSET (ALL|HARWARE|OS|DEVELOPMENT) [default all]\"
send \"\r\"
expect \"FASTMEM [default no]\"
send \"\r\"
expect \"SLOWFS [default no]\"
send  \"\r\"
expect \"DISKS [default none]\"
send \"\r\"
expect \"REMOTE [default none]\"
send  \"\r\"
expect \"Processor mhz [default 4171 MHz, 0.2398 nanosec clock]\"
send \"\r\"
expect \"FSDIR [default /usr/tmp]\"
send \"\r\"
expect \"Status output file [default /dev/tty]\"
send \"\r\"
expect \"\Mail results [default yes]\"
send \"\r\"
expect \"Editor [default vi]\"
send \"\r\"
expect eof
"
#expect \"\"
#send \"\r\"
#expect \"\"
#send  \"\r\"

