#!/bin/bash
##一键打包Linux服务包及依赖
##字体设置
#红色背景
red_bg='\e[41;33;1m'
#红色字体
red_zt='\e[1;31m'
#绿色背景
green_bg='\033[0;32;1m'
#绿色字体
green_zt='\e[1;32m'
#关闭效果
end='\033[0m'
#字体闪烁
ss='\033[05m'
##变量预配置
d='/Y'
rpms="${d}/rpm/"
debs='/var/cache/apt/archives'
zip_dir='/Y/zip'
mkdir -p $rpms $compress $zip_dir
zips(){
	if [ "$p" = 'yum' ];then
		f_nubmer=`ls  ${rpms} | wc -l`
		pushd ${rpms} > /dev/null
		files=${pack}_${r}_rpm.zip
		zip -r  ${files} ${pack} > /dev/null
	elif [ "$p" == 'apt' ];then
		 f_nubmer=`ls  $debs  | wc -l`
		pushd $debs > /dev/null
		files=${pack}_${r}_deb.zip
		zip -r ${files} *.deb > /dev/null
	fi
	mv  ${files} ${zip_dir}/
	if [ $? == '0' ];then
		echo -e "共打包${green_zt}${f_nubmer}${end}个服务包"
		echo -e "${green_zt}打包成功,已存放至:${end}"
		echo -e "${red_zt}${zip_dir}/${files} ${end}"
		size=`du -sh ${zip_dir}/${files}`
		echo -e "压缩包文件大小为: \n${green_zt}${size} ${end}"
	else
		echo -e "${red_bg} 打包失败 ${end}"
	fi
}
cheks(){
	if [ $? == "0" ];then
		echo -e "${green_zt} ${1}成功${end}"
		zips
	else
		echo -e " ${red_bg} ${1}失败 ${end}"
	fi
}
yum_package(){
	if [ ! -f '/usr/bin/zip' ];then
		yum install -y zip unzip -q
	fi
	pack="$1"
	echo -e "是否显示下载过程程${red_zt}[y/n]${end}"
	read v
	echo -e "正在下载${green_bg}${pack}${end},${ss}请耐心等待.....${end}"
	if [ "$v" == 'y' ];then
		yum install  --downloadonly --downloaddir=${rpms}/${pack} $pack
	else
		yum install  --downloadonly --downloaddir=${rpms}/${pack} $pack > /dev/null
	fi
	cheks "${pack}下载"
}
apt_package(){
	if [ ! -f '/usr/bin/zip' ];then
		apt-get install -y zip unzip
	fi
	rm -rf ${debs}/*
        pack="$1"
	echo -e "是否显示下载过程${red_zt}[y/n]${end}"
	read v
	echo -e "${ss}${green_zt}正在下载${pack},请耐心等待.......${end}"
	if [ "$v" == 'y' ];then
		apt-get -d -y install ${pack} 
	else
		apt-get -d -y install ${pack} > /dev/null
	fi
	cheks "${pack}下载"
}
##判断执行用户
if [[ $UID == "0" ]];then
	echo "当前使用ROOT用户"
else
	echo -e "当前非ROOT用户,建议切换至ROOT执行\n[按回车继续使用当前用户操作]"
	read Y
fi
#判断包管理器
if [ -f '/usr/bin/yum' ];then
	p='yum'
elif [ -f '/usr/bin/apt' ];then
	p='apt'
else
	echo '当前脚本暂不支持apt/yum之外的包管理器'
fi
if [ -z "$USER" ];then
	USER='仅支持Linux系统查询!'
fi
#信息列表
r=`uname -m`
echo -e "脚本配置信息如下:
当前管理器:${green_zt} ${p} ${end}
当前用户名:${green_zt} ${USER} ${end}
当前架构：${green_zt} ${r} ${end}
压缩包路径:${green_zt} ${zip_dir} ${end}"
#预打包配置
echo -e "当前${zip_dir}目录已有文件如下:"
for i in `ls ${zip_dir}`;do
	echo -e "${green_zt}${i}${end}"
done
echo -e  "是否删除${red_bg}数据:${end}[y/n]\n${ss}默认n${end}"
rm_dir(){
	if [ "$rms" == 'y' ];then
		rm -rf $debs ${z} ${rpms}
        	echo -e "${ss}${red_zt}删除完成${end}"
		mkdir -p $debs ${z} ${rpms}
	else
		echo -e "${green_zt}跳过删除${end}"
	fi
}
input(){
	echo -e "请输入需要安装的包名,多个服务请使用${red_bg}空格${end}分开\n例如：${green_zt}apache2 vsftpd${end}"
	read packs
}
if [ "$p" == "apt" ];then
	z=${zip_dir}/
	read rms
	rm_dir
	input
	for i in $packs;do
		f="${zip_dir}/${i}_${r}_deb.zip"
		if [ -f $f ];then
			echo -e "${green_zt}${i}${end}服务已打包,本次${red_zt}跳过下载${end}"
		else
			apt_package $i
		fi
		rm -rf ${debs}/*
	done
elif [ "$p" == "yum" ];then
	read rms
	rm_dir
	input
	for i in $packs;do
		f="${zip_dir}/${i}_${r}_rpm.zip"
		if [ -f $f ];then
			echo -e "${green_zt}${i}${end}服务已打包,本次${red_zt}跳过下载${end}"
		else
			yum_package $i
		fi
	done
fi
echo -e "当前压缩包目录${green_zt}文件列表${end}如下:"
du -sh ${zip_dir}/*.zip
cd ${zip_dir}
