#!/bin/bash
#设置显示器接口
Interface='VGA-1'
echo '请输入分辨率宽度
默认: 1920'
read witdh
echo '请输入分辨率宽度
默认: 1080'
read height
if [[ -z "${witdh}" ]];then
        witdh='1920'
fi
if [[ -z "${height}" ]];then
        height='1080'
fi
#设置分辨率
Resolving_power="${witdh} ${height}"
#新建分辨率选项
xrandr --newmode `cvt ${Resolving_power} | awk 'NR==2{print}' | sed 's/Modeline//'`
#添加分辨率选项到系统设置
xrandr --addmode  `cvt ${Resolving_power} | awk 'NR==2{print}' | awk '{print $2}'`