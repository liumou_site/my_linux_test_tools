#!/bin/bash
##USB性能测试
echo '请输入usb设备分区
例如:sdb1'
read dev
dir='/usbteset'
mkdir -p $dir
umount $dir
mount /dev/$dev $dir
if [[ $? == '0' ]];then
	echo '挂载成功,正在测试4k'
	dd if=/dev/zero of=/usbtest/usbtest bs=4k count=800000
	echo '正在测试8k'
	dd if=/dev/zero of=/usbtest/usbtest bs=8k count=400000
	echo '正在测试16k'
	dd if=/dev/zero of=/usbtest/usbtest bs=16k count=200000	
	echo '正在测试32k'
	dd if=/dev/zero of=/usbtest/usbtest bs=32k count=100000
	echo '正在测试64k'
	dd if=/dev/zero of=/usbtest/usbtest bs=64k count=50000
	echo '测试完成'
else
	echo '挂载失败'
fi
