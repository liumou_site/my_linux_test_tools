#!/bin/bash
##iozone v3_430
echo '请输入需要测试的硬盘挂载目录
格式：/ssd/iozone/
一定要在目录后面加  /'
read dd
dir="${dd}log/iozone/"
log="/log/iozone/"
rm -rf ${dir}*.xls
mkdir -p ${dir}
if [[ -f '/usr/bin/iozone' ]];then
	echo '正在检测本机内存...'
	ram=`free -g | awk '{print $2}' | awk 'NR==2{print}'`
	echo "本机内存: ${ram}G"
	echo "正在检测${dd}所在磁盘空间"
	rom=` df -h ${dd} | awk 'NR==2{print}' | awk '{print $4}' |sed 's/G//'`
	nuber="`expr $ram \* 2`"
	echo "指定测试空间剩余: ${rom}G"
	if [[ "$rom" -le "$nuber" ]];then
		echo '磁盘空间不足,请更换测试目录或尝试清理该目录'
	else
		echo '开始进入测试'
		echo -e "本机内存: ${ram}G\n设置块大小：16M"
		rams="`expr $ram / 2` $ram `expr $ram \* 2`"
		for i in $rams;do
			echo -e "\033[5;35m --------正在进行${i}G的测试-----------------\033[0m"
			iozone -a -i 0 -i 1 -i 2 -f  ${dir}${i}G.xls -r 16M -s ${i}G | tee -a ${log}${i}G.log
		done
		echo -e "测试完成,结果保存至\n${log}"
	fi
else
	echo -e "请先安装iozone,并创建软连接至:\n/usr/bin/iozone"
fi
