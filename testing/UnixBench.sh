#!/bin/bash
##UnixBench测试脚本
ydir='my_linux_test_tools/Source_code/unixbench/UnixBench/*'
tdir='/T/UnixBench/'
logs='/log/UnixBench/'
log_file="${logs}UnixBench.log"
rm -rf ${logs} ${tdir}
mkdir -p ${logs} ${tdir}
tests(){
	echo '是否完全后台运行[y/n],默认n'
	read ht
	if [ "$ht" == 'y' ];then
		echo -e "测试结果将保存至:\n${logs}"
		hts='1'
	else
		echo -e "测试记录将保存至:\n${logs}"
		hts='2'
	fi
	echo '复制源码'
	cp -ri $ydir $tdir
	make
	for((i=1;i<=3;i++));do
		pushd ${tdir}
		cd ${tdir}
		touch ${log_file}
		if [[ "$hts" == "1" ]];then
			./Run > ${log_file}.${i}
		else
			./Run | tee -a ${log_file}.${i}
		fi
	done
	echo '程序运行结束'
}
chek(){
	if [ -f '/usr/bin/gcc' ];then
		echo 'gcc已安装'
		tests
	else
		echo '请先安装gcc'
		exit 1
	fi
}
chek
