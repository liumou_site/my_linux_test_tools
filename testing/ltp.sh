#!/bin/bash
echo -e "请输入LTP安装程序的绝对路径\n默认：/opt/ltp/\n路径后面必须加斜杠: /"
read dir
save_dir='/log/ltp'
rm -rf $save_dir
mkdir -p $save_dir
if [ -z "$dir" ];then
	dir='/opt/ltp/'
	echo "使用${dir}目录"
    mkdir -p $dir
else
	echo -e "正在检测"
fi
if [[ -d "$dir" ]];then
	echo -e "ltp安装路径已确认\n正在检测是否成功安装"
	if [[ -f "${dir}testscripts/ltpstress.sh" ]];then
		echo -e "请输入需要测试的时长[单位小时]"
		read ti
		file=$(date '+%Y-%D%H' | sed 's#/#-#g')
		echo '测试以后台形式进行，当看到 /log/ltp/done.info文件存在，且内容为done的时候说明测试完成'
		cd ${dir}testscripts
		bash ${dir}testscripts/ltpstress.sh -n -t $ti ${save_dir}/ltp${file}.log&
		echo 'done' > /log/ltp/done.info
	else
		echo -e "找不到测试脚本\n请确认LTP是否已安装成功并安装在${dir}"
	fi		
else
	echo -e "${dir}目录不存在"
fi
