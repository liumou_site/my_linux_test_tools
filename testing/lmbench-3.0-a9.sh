#!/bin/bash
chek(){
	if [[ $? == '0' ]];then
		echo "$1\t执行成功"
	else
		echo "$1\t执行失败"
	fi
}
echo '请输入lmbench-3.0-a9绝对路径'
read dir
if [ -d "$dir" ];then
	cd $dir
	ls results|grep -vi Makefile|rm -rf
	chek 'rm Makefile'
    make clean
	chek 'clean'
	make results
	chek 'results'
    for((i=2;i<=3;i++));do
            make rerun
            chek "第${i}次测试"
    done	
else
	echo '路径不存在'
fi