#!/bin/bash
chmod +x run-specjvm.sh
save_dir="/log/SPECjvm2008/"
if [[ -d "$save_dir" ]];then
	echo "发现保存目录${save_dir}已存在,若需要备份资料请终止此程序
输入   'r'  删除该目录下所有文件,按回车退出程序"
	read dd
	if [[ "$dd" == "r" ]];then
		rm -rf ${save_dir}
	else
		echo '程序终止'
		exit 1
	fi
fi
mkdir -p $save_dir
echo "测试结果保存在${save_dir}目录,测试过程可以去该目录查看结果"
if [[ -f "run-specjvm.sh" ]];then
	echo '正在进入测试'
	jvm="startup.helloworld startup.compiler.compiler startup.compiler.sunflow startup.compress
	startup.crypto.aes startup.crypto.rsa startup.crypto.signverify startup.mpegaudio startup.scimark.fft
	startup.scimark.lu startup.scimark.monte_carlo startup.scimark.sor startup.scimark.sparse startup.serial
	startup.sunflow startup.xml.transform startup.xml.validation compiler.compiler compiler.sunflow	
	compress crypto.aes crypto.rsa crypto.signverify derby BigDecimal mpegaudio scimark.fft.large scimark.lu.large
	scimark.sor.large  scimark.sparse.large scimark.fft.small scimark.lu.small scimark.sor.small scimark.sparse.small
	scimark.monte_carlo serial sunflow xml.transform xml.validation"
	for type in $jvm;do
		echo -e "\n当前测试项:${type}\n程序进程:$$"
		echo -e "当前测试项结果结束后将记录至:\n /log/SPECjvm2008/${type}.log"
		echo -e "如需终止请在另一个终端执行:\nkill -9 $$\n"
		touch /log/SPECjvm2008/${type}.log
		bash run-specjvm.sh $type -ikv > /log/SPECjvm2008/${type}.log
	done
else
	dir=`pwd`
	dirs="${dir}my_linux_test_tools/testing/SPECjvm2008.sh"
	echo "请将${dirs}复制到SPECjvm2008到安装目录"
fi
