#!/bin/bash
dir='/log/netperf'
echo '请输入对端IP地址'
read d_ip
echo -e "测试记录保存路径为:${dir}\n按回车清空${dir}目录"
read dd
echo '正在删除'
mkdir -p $dir
echo '正在检测对端是否能够连接'
ping $d_ip -c 5 > /dev/null
if [ $? == '0' ];then
    echo '对端连接成功'
	for((i=1;i<=3;i++));do
        echo "正在进行第${i}轮测试"
        netperf -t TCP_CRR -H ${d_ip} -l 60 | tee -a ${dir}/TCP_CRR${i}
		netperf -t TCP_RR -H ${d_ip} -l 60 | tee -a ${dir}/TCP_RR${i}
		netperf -t TCP_STREAM -H ${d_ip} -l 60 | tee -a ${dir}/TCP_STREAM${i}
		netperf -t UDP_RR -H ${d_ip} -l 60 | tee -a ${dir}/UDP_RR${i}
		netperf -t UDP_STREAM -H ${d_ip} -l 60 | tee -a ${dir}/UDP_STREAM${i}
     done
	echo "测试完成"
    echo -e "测试结果保存目录:
cd ${dir}/"
else
        echo '对端连接失败,程序终止'
fi  
