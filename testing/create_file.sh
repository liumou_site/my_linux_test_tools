#!/bin/bash
size="512 1024 5120"
bs="1M"
rm -rf file/
mkdir file/
echo '请确认是否已将测试脚本放至需要测试的磁盘目录
按回车默认测试当前路径的磁盘'
read dd
echo "BS大小为1M"
for i in $size;do
	echo -e "当前块大小为:${i}M"
	dd if=/dev/zero of=file/file_${i} bs=$bs count=$i
	if [[ "$?" == "0" ]];then
		echo "文件创建成功"
		a="成功"
	else
		echo "文件创建失败"
		a="失败"
	fi
	cp file/file_${i} file/file_${i}.cp
	if [[ "$?" == "0" ]];then
		 echo -e "文件复制成功,复制文件为:\nfile/file_${i}.cp"
		 b="成功"
	else
		echo "文件复制失败"
		b="失败"
	fi
	tar -cf file/file_${i}.tar file/file_${i}
	if [[ "$?" == "0" ]];then
		echo "文件压缩成功"
		c="成功"
	else
		echo '文件压缩失败,请手动测试'
		c="失败"
	fi
	cp file/file_${i} file/file_${i}.create
	tar -xvf file/file_${i}.tar
	if [[ "$?" == "0" ]];then
		echo "解压成功"
		d="成功"
	else
		echo "解压失败"
		d="失败"
	fi
	echo -e "${i}M文件测试结果:
创建:${a}
复制:${b}
压缩:${c}
解压:${d}"
	echo "文件详情如下:"
	du -sh file/*
	echo '测试文件已删除'
	rm -rf file/*
done
