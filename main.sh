#!/bin/bash
##Linux测试脚本集合主脚本
##测试方向
direction_test(){
	echo '请选择测试方向
[1]功能测试
[2]兼容性测试
[3]功能测试
[4]稳定性测试
[5]安全测试
[6]其他测试'
read directions
}

##功能测试
function_test(){
	echo '进入功能性测试'
	read t
}
performance(){
echo '请选择需要测试的类型
[1]CPU性能测试
[2]磁盘性能测试
[3]网络性能测试
[4]内存性能测试
[a]全部测试'
	read type
	expr $type "+" 10 >> /dev/null
	if [ $? -eq '0' ];then
		echo '这是数字'
		case "$type" in
			"1") echo "1"
			;;
			"2") echo '2'
			;;
			"3") echo '3'
			;;
			"4") echo '4'
			;;
		esac
	else
		if [[ "$type" == "A" || "$type" == 'a' ]];then
			echo 'use all'
		else
			echo 'select errors'
		fi
	fi
}

#系统信息检测
os_ver(){
	echo -e "正在检测系统信息\n正在检测系统架构"
	Framework=`uname -m`
	echo '正在检测包类型'
	if [[  -f "/usr/bin/yum" ]];then
		package='rpm'
	elif [[ -f "/usr/bin/apt" ]];then
		package='deb'
	elif [[ -f "/usr/bin/apt-get" ]];then
		package='deb'
	else
		echo -e "无法判断该系统使用的包,请选择
[1]rpm
[2]deb"
		read p
		if [[ "$p" == "1" ]];then
			package='rpm'
		elif [[  "$p" == "2" ]];then
			package='deb'
		else
			echo '选择错误'
			exit 1
		fi
	fi
	echo '正在检测发行版'
	ver=`cat /etc/os-release | grep 'PRETTY_NAME' | sed 's/PRETTY_NAME="//'| awk '{print $1}'`
	echo -e "本机系统架构:	\033[31m${Framework}\033[0m
使用的默认包类型:	\033[35m${package}\033[0m
当前Linux发行版:	\033[35m${ver}\033[0m
其他:	待更新"
	echo -e "\033[5;35m确认无误请回车\033[0m"
	read dd
}
##脚本第一运行块
echo '请选择想要的操作
[1]我已经安装测试工具，我要直接测试
[2]我没有安装测试工具，我要安装工具[更新较慢]
[3]我要直接测试'
read op
if [[ "$op" == "1" ]];then
        op='testing'
elif [[ "$op" == "2" ]];then
        op='auto_shell'
elif [[ "$op" == "3" ]];then
        op='testing'
else
    echo '选择错误'
fi
os_ver
if [[ "$fx" == "1" ]];then
	performance
elif [[ "$fx" == "2" ]];then
	tools
elif [[ "$fx" == "3" ]];then
	functional
else
	echo '选择错误'
fi
