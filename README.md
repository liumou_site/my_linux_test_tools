# 我的Linux测试工具

## 介绍
汇集各类涉及Linux系统的测试工具

## 使用说明

### 获取脚本内容
使用下面的命令克隆本项目到你的本地(需要安装git工具)
```bash
git clone https://gitee.com/with_chen_ya/my_linux_test_tools.git
```
然后进入项目目录
```bash
cd my_linux_test_tools/
```
最后执行主脚本即可
```bash
bash main.sh
```